import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ErpCardComponent } from './erp-card.component';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [ErpCardComponent],
  imports: [CommonModule, HttpClientModule, MatIconModule],
  exports: [ErpCardComponent],
})
export class ErpCardModule {}
