import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErpCardComponent } from './erp-card.component';

describe('ErpCardComponent', () => {
  let component: ErpCardComponent;
  let fixture: ComponentFixture<ErpCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ErpCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ErpCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
