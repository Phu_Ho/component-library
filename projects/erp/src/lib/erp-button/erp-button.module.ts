import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ErpButtonComponent } from './erp-button.component';

@NgModule({
  declarations: [ErpButtonComponent],
  imports: [CommonModule],
  exports: [ErpButtonComponent],
})
export class ErpButtonModule {}
