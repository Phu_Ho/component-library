import type { Meta, StoryObj } from '@storybook/angular';
import { ErpButtonComponent } from 'projects/erp/src/lib/erp-button/erp-button.component';

const meta: Meta<ErpButtonComponent> = {
  title: 'Example/Button',
  component: ErpButtonComponent,
  render: (args: ErpButtonComponent) => ({
    props: {
      ...args,
    },
  }),
};

export default meta;
type Story = StoryObj<ErpButtonComponent>;

export const Default: Story = {
  args: {
    text: 'text default',
    background: 'secondary',
  },
};

export const Primary: Story = {
  args: {
    text: 'text primary',
    background: '#4F2566',
  },
};
