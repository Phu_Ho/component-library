import type { Meta, StoryObj } from '@storybook/angular';
import { ErpCardComponent } from 'projects/erp/src/lib/erp-card/erp-card.component';
import { erpStoryWrapper } from './utils';

const meta: Meta<ErpCardComponent> = {
  title: 'Example/Card',
  component: ErpCardComponent,
  render: (args: ErpCardComponent) => ({
    props: {
      ...args,
    },
  }),
};

export default meta;
type Story = StoryObj<ErpCardComponent>;

const defaultExample = `<lib-erp-card>
  <p content>
    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Officia nemo
    atque ea odio harum earum laborum quasi, deserunt ut ipsam illum quod,
    sapiente nobis quia tenetur voluptatibus facere! Quam, qui?
  </p>
</lib-erp-card>`;

export const Default = (args: ErpCardComponent) => ({
  props: { ...args },
  template: erpStoryWrapper(defaultExample),
});
